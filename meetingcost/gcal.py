#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : gcal.py
# Author      : Tushar Saxena (tushar@flipkart.com)
# Date        : 07-Jun-2012
# Description : Library for connecting to the Google Calendar API
# -----------------------------------------------------------------------------

import os, sys, re
from datetime import *

import gdata.data
import gdata.calendar.data
import gdata.calendar.client

# Globals
BASE_SALARY = 1500000.0
COST_PER_HOUR = round((BASE_SALARY/12.0/22.0/8.0),2)
DAY_OF_WEEK = {1:'Monday',2:'Tuesday',3:'Wednesday',4:'Thursday',5:'Friday',6:'Saturday',7:'Sunday'}
MAX_FAVORITES_COUNT = 20
MAX_RESULTS = 9999
DEBUG = True

# --------------------------------------------------------------------------------------------------
class CalendarHelper:
# --------------------------------------------------------------------------------------------------

	client = None
	request_token = None
	request_token_refresh = None
	access_token = None

	isAuthenticated = None

	username = None
	password = None
	
	DEFAULT_SOURCE = 'GoogleCalendarHelper'
	SCOPES = ['https://www.google.com/calendar/feeds/']

	CONSUMER_KEY = '113610691848.apps.googleusercontent.com'
	CONSUMER_SECRET = 'jxzrqlYkWrUAzy0iNGAUhNLw'

	"""
	# -------------------------------------
	def __init__(self, username, password):
	# -------------------------------------

		self.username = username
		self.password = password
		
		# Init docs client
		self.client = gdata.calendar.client.CalendarClient(source=self.DEFAULT_SOURCE)
	"""

	# -------------------------------------
	def __init__(self):
	# -------------------------------------

		self.request_token = None
		self.request_token_refresh = None
		self.access_token = None
		self.isAuthenticated = False
		
		# Init calendar client
		self.client = gdata.calendar.client.CalendarClient(source=self.DEFAULT_SOURCE)

	# -------------------------------------
	def getOAuthToken(self, callback_url):
	# -------------------------------------

		self.request_token = self.client.GetOAuthToken(self.SCOPES,callback_url,consumer_key=self.CONSUMER_KEY,consumer_secret=self.CONSUMER_SECRET)
		redirect_url = self.request_token.generate_authorization_url()

		return redirect_url

	# -------------------------------------
	def getAccessToken(self, request_uri):
	# -------------------------------------

		self.request_token_refresh = gdata.gauth.AuthorizeRequestToken(self.request_token,request_uri)

		try:
			self.access_token = self.client.GetAccessToken(self.request_token_refresh)
		except Exception,e:
			print e
			return False

		self.client.auth_token = self.access_token
		self.isAuthenticated = True

		return True

	# -------------------------------------
	def setUsername(self):
	# -------------------------------------

		feed = self.client.GetOwnCalendarsFeed()
		self.username = feed.entry[0].author[0].email.text

		return True

	# -------------------------------------
	def authenticated(self):
	# -------------------------------------

		return self.isAuthenticated

	# -------------------------------------
	def getFeed(self, start_date=None, end_date=None, email='', text_query=''):
	# -------------------------------------

		if self.isAuthenticated == False:		
			return None

		# Build model & metrics
		model = self.__buildModel__(start_date,end_date,email,text_query)
		metrics = self.__buildMetrics__(model,email)

		return (model,metrics)

	# -------------------------------------
	def __buildModel__(self, start_date, end_date, email='', text_query=''):
	# -------------------------------------

		text_query = text_query.lower().strip()
		
		# Build query URI
		if email == '' or email==self.username:
			email = 'default'
		else:
			email = email.replace('@','%40')

		query = gdata.calendar.client.CalendarEventQuery(start_min=str(start_date),start_max=str(end_date),max_results=MAX_RESULTS)
		
		# Retrieve feed
		base_uri = 'https://www.google.com/calendar/feeds/%s/private/full' % (email)
		try:
			feed = self.client.GetCalendarEventFeed(uri=base_uri,q=query)
		except Exception,e:
			print e
			return []
		
		# Tokenize text_query
		positive_tokens = []
		negative_tokens = []
		text_query = text_query.lower().strip()

		positive_tokens_raw = re.findall(r"[\w']+", text_query)
		negative_tokens_raw = re.findall(r"-[\w']+", text_query)

		for token in negative_tokens_raw:
			negative_tokens.append(token[1:])
		for token in positive_tokens_raw:
			if token not in negative_tokens:
				positive_tokens.append(token)
		
		# Loop through feed
		googleCalendarEntryList = []
		while feed:
			for entry in feed.entry:
				if self.__checkSearchText__(title=entry.title.text,description=entry.content.text,positive_tokens=positive_tokens,negative_tokens=negative_tokens):
					for when in entry.when:
						event = CalendarEventEntry(entry,when)
						if event.status not in ['event.canceled']:
							googleCalendarEntryList.append(event)
			try:
				feed = self.client.GetNext(feed)
			except Exception,e:
				break

		sorted_list = sorted(googleCalendarEntryList, key=lambda k: k.start_time)

		# Sort list by start time
		return sorted_list

	# -------------------------------------
	def __checkSearchText__(self, title='', description='', positive_tokens=[], negative_tokens=[]):
	# -------------------------------------

		if len(positive_tokens) == 0 and len(negative_tokens) == 0:
			return True
		else:

			if title: title = title.lower().strip()
			else: title = ''
	
			if description: description = description.lower().strip()
			else: description = ''


			positive_match = False
			negative_match = False
			
			for token in positive_tokens:
				if title.find(token) != -1 or description.find(token) != -1:
					positive_match = True

			for token in negative_tokens:
				if title.find(token) != -1 or description.find(token) != -1:
					negative_match = True

			if negative_match == False and positive_match == True:
				return True
			else:
				return False
	
	# -------------------------------------
	def __buildMetrics__(self, model, email):
	# -------------------------------------

		if model == None:
			return None

		if email == '':
			email==self.username

		# Init
		metrics = CalendarMetrics()
		
		favorite_organizer_dict = {}
		favorite_invitee_dict = {}
		favorite_coattendee_dict = {}
		
		weekofyear_dict = {}
		dayofweek_dict = {}
		
		# Loop through all events and build metrics dictionary
		for event in model:

			event.buildMetrics()
			
			# Overalls
			metrics.count += 1
			metrics.cost += event.metrics.cost
			metrics.time += event.metrics.duration

			# Week of Year
			woy_key = '%4d|%2d' % (event.start_time.isocalendar()[0],event.start_time.isocalendar()[1])
			if weekofyear_dict.has_key(woy_key) == False:
				weekofyear_dict[woy_key] = {'count':0.0,'cost':0.0,'time':0.0}
			weekofyear_dict[woy_key]['count'] += 1
			weekofyear_dict[woy_key]['cost'] += event.metrics.cost
			weekofyear_dict[woy_key]['time'] += event.metrics.duration

			# Day of Week
			dow_key = event.start_time.isocalendar()[2]
			if dayofweek_dict.has_key(dow_key) == False:
				dayofweek_dict[dow_key] = {'count':0.0,'cost':0.0,'time':0.0,'week_list':[]}
			dayofweek_dict[dow_key]['count'] += 1
			dayofweek_dict[dow_key]['cost'] += event.metrics.cost
			dayofweek_dict[dow_key]['time'] += event.metrics.duration
			if woy_key not in dayofweek_dict[dow_key]['week_list']:
				dayofweek_dict[dow_key]['week_list'].append(woy_key)

			if email == event.organizer.email:
				# Current user was organizer
				metrics.count_organized += 1
				metrics.cost_organized += event.metrics.cost
				metrics.time_organized += event.metrics.duration
			else:
				# Current user was NOT organizer
				# Favorite organizer
				if favorite_organizer_dict.has_key('%s|%s'%(event.organizer.email,event.organizer.name)) == False:
					favorite_organizer_dict['%s|%s'%(event.organizer.email,event.organizer.name)] = 0
				favorite_organizer_dict['%s|%s'%(event.organizer.email,event.organizer.name)] += 1
				# Favorite Co-attendee						
				if favorite_coattendee_dict.has_key('%s|%s'%(event.organizer.email,event.organizer.name)) == False:
					favorite_coattendee_dict['%s|%s'%(event.organizer.email,event.organizer.name)] = 0
				favorite_coattendee_dict['%s|%s'%(event.organizer.email,event.organizer.name)] += 1
			
			# !Organizer, Loop through Attendees
			for attendee in event.attendees:
				if email == attendee.email:
					# Current user was invited
					if attendee.status == 'accepted':
						metrics.count_accepted += 1
						metrics.cost_accepted += event.metrics.cost
						metrics.time_accepted += event.metrics.duration
					elif attendee.status == 'invited':
						metrics.count_invited += 1
						metrics.cost_invited += event.metrics.cost
						metrics.time_invited += event.metrics.duration
					elif attendee.status == 'tentative':
						metrics.count_tentative += 1
						metrics.cost_tentative += event.metrics.cost
						metrics.time_tentative += event.metrics.duration
					elif attendee.status == 'declined':
						metrics.count_declined += 1
						metrics.cost_declined += event.metrics.cost
						metrics.time_declined += event.metrics.duration
				else:
					if attendee.type == 'person':
						# Favorite Invitee
						if email == event.organizer.email:
							if favorite_invitee_dict.has_key('%s|%s'%(attendee.email,attendee.name)) == False:
								favorite_invitee_dict['%s|%s'%(attendee.email,attendee.name)] = 0
							favorite_invitee_dict['%s|%s'%(attendee.email,attendee.name)] += 1
						# Favorite Co-attendee						
						if favorite_coattendee_dict.has_key('%s|%s'%(attendee.email,attendee.name)) == False:
							favorite_coattendee_dict['%s|%s'%(attendee.email,attendee.name)] = 0
						favorite_coattendee_dict['%s|%s'%(attendee.email,attendee.name)] += 1

		# Day of Week - Averages
		for k,v in dayofweek_dict.iteritems():
			for variable in ['count','cost','time']:
				dayofweek_dict[k][variable] = round(dayofweek_dict[k][variable]/len(dayofweek_dict[k]['week_list']),2)

		counter = 0
		for key,count in sorted(favorite_organizer_dict.items(), key=lambda (key,value): value, reverse=True):
			counter += 1
			if counter > MAX_FAVORITES_COUNT: break
			email,name = key.split('|')
			favorite_entry = FavoriteEntry(name,email,count)
			metrics.favorite_organizer.append(favorite_entry)

		counter = 0
		for key,count in sorted(favorite_invitee_dict.items(), key=lambda (key,value): value, reverse=True):
			counter += 1
			if counter > MAX_FAVORITES_COUNT: break
			email,name = key.split('|')
			favorite_entry = FavoriteEntry(name,email,count)
			metrics.favorite_invitee.append(favorite_entry)

		counter = 0
		for key,count in sorted(favorite_coattendee_dict.items(), key=lambda (key,value): value, reverse=True):
			counter += 1
			if counter > MAX_FAVORITES_COUNT: break
			email,name = key.split('|')
			favorite_entry = FavoriteEntry(name,email,count)
			metrics.favorite_coattendees.append(favorite_entry)

		# Week of Year
		for key,value in sorted(weekofyear_dict.items(), key=lambda (key,value): key):
			(year,week) = key.split('|')
			week = int(week)
			count = value['count']
			cost = value['cost']
			time_ = value['time']
			weekofyear_entry = WeekofYearEntry(year,week,count,cost,time_)
			metrics.weekofyear.append(weekofyear_entry)

		# Day of Week
		for key,value in sorted(dayofweek_dict.items(), key=lambda (key,value): key):
			day = key
			count = value['count']
			cost = value['cost']
			time_ = value['time']
			dayofweek_entry = DayofWeekEntry(day,count,cost,time_)
			metrics.dayofweek.append(dayofweek_entry)

		return metrics
		
# --------------------------------------------------------------------------------------------------
class FavoriteEntry:
# --------------------------------------------------------------------------------------------------

	# -------------------------------------
	def __init__(self, name=None, email=None, value=None):
	# -------------------------------------
		
		self.name = name
		self.email = email
		self.value = value

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

# --------------------------------------------------------------------------------------------------
class WeekofYearEntry:
# --------------------------------------------------------------------------------------------------

	# -------------------------------------
	def __init__(self, year=None, week=None, count=None, cost=None, time_=None):
	# -------------------------------------
		
		self.year = year
		self.week = week
		self.count = count
		self.cost = cost
		self.time_ = time_

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

# --------------------------------------------------------------------------------------------------
class DayofWeekEntry:
# --------------------------------------------------------------------------------------------------

	# -------------------------------------
	def __init__(self, day=None, count=None, cost=None, time_=None):
	# -------------------------------------
		
		self.day = DAY_OF_WEEK[day]
		self.count = count
		self.cost = cost
		self.time_ = time_

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

# --------------------------------------------------------------------------------------------------
class CalendarMetrics:
# --------------------------------------------------------------------------------------------------

	# -------------------------------------
	def __init__(self):
	# -------------------------------------

		# Count
		self.count = 0.0
		self.count_organized = 0.0
		self.count_accepted = 0.0
		self.count_declined = 0.0
		self.count_invited = 0.0
		self.count_tentative = 0.0
		
		# Cost
		self.cost = 0.0
		self.cost_organized = 0.0
		self.cost_accepted = 0.0
		self.cost_invited = 0.0
		self.cost_tentative = 0.0
		self.cost_declined = 0.0
		
		# Time
		self.time = 0.0
		self.time_organized = 0.0
		self.time_accepted = 0.0
		self.time_invited = 0.0
		self.time_tentative = 0.0
		self.time_declined = 0.0
		
		# Favorites
		self.favorite_organizer = []	
		self.favorite_invitee = []	
		self.favorite_coattendees = []	

		# Week of Year & Day of Week
		self.weekofyear = []
		self.dayofweek = []

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

# --------------------------------------------------------------------------------------------------
class CalendarEventMetrics:
# --------------------------------------------------------------------------------------------------

	# -------------------------------------
	def __init__(self):
	# -------------------------------------

		self.attendees = 0
		self.accepted = 0
		self.declined = 0
		self.invited = 0
		self.tentative = 0
		self.cost = 0.0
		self.duration = 0.0

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

# --------------------------------------------------------------------------------------------------
class CalendarEventEntry:
# --------------------------------------------------------------------------------------------------

	entry = None
	id = None
	uri = None
	title = None
	description = None
	start_time = None
	end_time = None
	status = None
	organizer = None
	attendees = []
	metrics = None
	
	# -------------------------------------
	def __init__(self, entry, when):
	# -------------------------------------

		self.entry = entry
		self.id = entry.id.text.split('/')[-1]
		self.title = entry.title.text
		self.description = entry.content.text
		self.start_time = self.__convertGoogleTimestamp(when.start)
		self.end_time = self.__convertGoogleTimestamp(when.end)
		self.status = entry.event_status.value.split('#')[-1]
		self.attendees = []

		for link in entry.link:
			if link.rel == 'alternate':
				self.url = link.href
						
		for attendee in entry.who:
			if attendee.attendee_status:
				rel = attendee.rel.split('#')[-1]
				if rel == 'event.organizer':
					self.organizer = CalendarAttendeeEntry(attendee)
				elif rel == 'event.attendee':
					attendee_entry = CalendarAttendeeEntry(attendee)
					self.attendees.append(attendee_entry)	

		if self.organizer == None:
			self.organizer = CalendarOrganizerEntry(entry)
		
		self.attendees = sorted(self.attendees, key=lambda k: k.status + k.type + k.email)
		self.metrics = CalendarEventMetrics()

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

	# --------------------------------------------------------------------------------------------------
	def buildMetrics(self):
	# --------------------------------------------------------------------------------------------------

		# Attendee Status (Count)
		for attendee in self.attendees:
			if attendee.status == 'accepted':
				self.metrics.accepted += 1
			if attendee.status == 'invited':
				self.metrics.invited += 1
			if attendee.status == 'declined':
				self.metrics.declined += 1
			if attendee.status == 'tentative':
				self.metrics.tentative += 1

		if len(self.attendees) == 0:
			self.metrics.attendees = 1 #Organizer only
		else:
			self.metrics.attendees = len(self.attendees) + 1 #Attendees + Organizer
		
		# Cost
		self.metrics.duration = float((self.end_time - self.start_time).total_seconds())/60.0/60.0
		self.metrics.cost = round((self.metrics.accepted + self.metrics.invited + 1) * self.metrics.duration * COST_PER_HOUR,2)

	# --------------------------------------------------------------------------------------------------
	def isCurrent(self):
	# --------------------------------------------------------------------------------------------------
		
		return True
		
	# --------------------------------------------------------------------------------------------------
	def __convertGoogleTimestamp(self, string):
	# --------------------------------------------------------------------------------------------------
	
		if len(string) != 29:
			string = string + 'T00:00:00'

		return datetime.strptime(string[0:19],'%Y-%m-%dT%H:%M:%S')

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

# --------------------------------------------------------------------------------------------------
class CalendarOrganizerEntry:
# --------------------------------------------------------------------------------------------------

	# -------------------------------------
	def __init__(self, entry):
	# -------------------------------------

		self.name = entry.author[0].name.text
		if entry.author[0].email:
			self.email = entry.author[0].email.text
		else:
			self.email = 'N/A'
		if self.email.endswith('@resource.calendar.google.com'):
			self.type = 'resource'
		else:
			self.type = 'person'
		self.status = 'accepted'

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

# --------------------------------------------------------------------------------------------------
class CalendarAttendeeEntry:
# --------------------------------------------------------------------------------------------------

	# -------------------------------------
	def __init__(self, entry):
	# -------------------------------------

		self.name = entry.value
		self.email = entry.email
		if entry.email.endswith('@resource.calendar.google.com'):
			self.type = 'resource'
		else:
			self.type = 'person'
		self.status = entry.attendee_status.value.split('#')[-1].split('.')[-1]

	# -------------------------------------
	def __str__(self):
	# -------------------------------------

		return str(vars(self))

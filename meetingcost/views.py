import time as time_old
import logging

from datetime import *
from gcal import CalendarHelper
from mysettings import *
from chartdata import *

from django.template import Context, loader
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django import forms
from django.forms.fields import DateField
from django.forms.extras.widgets import SelectDateWidget

# Logging Config
LOG_FORMAT = '%(clientip)-20s | %(username)-30s | %(method)-20s | %(message)s'
logging.basicConfig(format=LOG_FORMAT)

#from meeting.wsgi import DJANGO_PROJECT_PATH
#log_handler = logging.FileHandler(DJANGO_PROJECT_PATH + 'logs/meetingcost.log')
#log_formatter = logging.Formatter(LOG_FORMAT)
#log_handler.setFormatter(log_formatter)

logger = logging.getLogger('meeting.meetingcost')
#logger.addHandler(log_handler) 
logger.setLevel(logging.INFO)

# -----------------------------------------------------------------------------
def root(request):
# -----------------------------------------------------------------------------

	return HttpResponseRedirect('/dashboard')

# -----------------------------------------------------------------------------
def getRemoteAddress(request):
# -----------------------------------------------------------------------------

	remote_address = None

	if request.META.has_key('HTTP_X_FORWARDED_FOR'):
		remote_address = request.META['HTTP_X_FORWARDED_FOR']
	elif request.META.has_key('REMOTE_ADDR'):
		remote_address = request.META['REMOTE_ADDR']

	return remote_address

# -----------------------------------------------------------------------------
def getUsername(request):
# -----------------------------------------------------------------------------
	
	username = None

	calendar = request.session.get('calendar', None)
	if calendar:
		username = calendar.username
	
	return username

# -----------------------------------------------------------------------------
def login(request):
# -----------------------------------------------------------------------------

	calendar = request.session.get('calendar', None)
	if calendar and calendar.authenticated():
		return HttpResponseRedirect('/dashboard')

	request.session['calendar'] = None

	calendar = CalendarHelper()
	callback_url = 'http://%s/login/callback' % request.get_host()
	redirect_url = calendar.getOAuthToken(callback_url)
	if redirect_url == None:
		return HttpResponseRedirect('/login')
	
	log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/login'}
	log_message = None
	logger.info(log_message,extra=log_params)

	request.session['calendar'] = calendar
	return render_to_response('login.template',{'redirect_url': redirect_url,})	
	
# -----------------------------------------------------------------------------
def login_callback(request):
# -----------------------------------------------------------------------------

	calendar = request.session.get('calendar', None)
	if calendar == None:
		return HttpResponseRedirect('/login')

	status = calendar.getAccessToken(request.build_absolute_uri())
	if status != True:
		return HttpResponseRedirect('/login')
		
	status = calendar.setUsername()
	if status != True:
		return HttpResponseRedirect('/login')

	log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/login/callback'}
	log_message = None
	logger.info(log_message,extra=log_params)

	request.session['calendar'] = calendar
	
	return HttpResponseRedirect('/dashboard')

# -----------------------------------------------------------------------------
def logout(request):
# -----------------------------------------------------------------------------

	calendar = request.session.get('calendar', None)

	log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/logout'}
	log_message = None
	logger.info(log_message,extra=log_params)

	if calendar == None or calendar.authenticated() == False:
		return HttpResponseRedirect('/login')

	return render_to_response('logout.template',{})
	
# -----------------------------------------------------------------------------
def logout_purge(request):
# -----------------------------------------------------------------------------

	log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/logout/purge'}
	log_message = None
	logger.info(log_message,extra=log_params)

	request.session['calendar'] = None
	request.session['model'] = None
	request.session['metrics'] = None
	request.session['resource_model'] = None
	request.session['resource_metrics'] = None
	request.session['compare_model'] = None
	request.session['compare_metrics1'] = None
	request.session['compare_metrics2'] = None
	request.session['compare_email1'] = None
	request.session['compare_email2'] = None

	return HttpResponseRedirect('/login')

# -----------------------------------------------------------------------------
def dashboard(request):
# -----------------------------------------------------------------------------

	calendar = request.session.get('calendar', None)
	if calendar == None or calendar.authenticated() == False:
		return HttpResponseRedirect('/login')

	username = calendar.username
	email = None

	if request.method == 'GET':
		form = DashboardForm()
	elif request.method == 'POST':
		request.session['model'] = None
		request.session['metrics'] = None
		form = DashboardForm(request.POST)
		if form.is_valid():
			start_date = form.cleaned_data['start_date']
			end_date = form.cleaned_data['end_date'] + timedelta(days=1)
			text_query = form.cleaned_data['text_query']
			email = form.cleaned_data['email']
			if email.strip() == '':
				email = username
			if (end_date < start_date) == True:
				message = 'End date can not be less than start date'
				return render_to_response('dashboard.template',{'form': form,'username':username,'email':email})
			t_start = time_old.time()
			(model,metrics) = calendar.getFeed(start_date=start_date,end_date=end_date,email=email,text_query=text_query)
			t_end = time_old.time()
			execution_time = '%.5f' % (t_end-t_start)
			log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/dashboard'}
			log_message = '[email] %s [text_query] %s [start_date] %s [end_date] %s [entries] %d [execution_time] %s seconds' % (email,text_query,start_date,end_date,len(model),execution_time)
			logger.info(log_message,extra=log_params)
			request.session['model'] = model
			request.session['metrics'] = metrics
			name = getNameFromModel(email,model)
			return render_to_response('dashboard.template', {'form': form, 'model': model,'metrics':metrics,'username':username,'execution_time':execution_time,'name':name})
			
	return render_to_response('dashboard.template',{'form': form,'username':username,'email':email})

# -----------------------------------------------------------------------------
def resource(request):
# -----------------------------------------------------------------------------

	calendar = request.session.get('calendar', None)
	if calendar == None or calendar.authenticated() == False:
		return HttpResponseRedirect('/login')

	username = calendar.username
	email = None

	if request.method == 'GET':
		form = ResourceForm()
	elif request.method == 'POST':
		request.session['resource_model'] = None
		request.session['resource_metrics'] = None
		form = ResourceForm(request.POST)
		if form.is_valid():
			start_date = form.cleaned_data['start_date']
			end_date = form.cleaned_data['end_date'] + timedelta(days=1)
			text_query = form.cleaned_data['text_query']
			resource = form.cleaned_data['resource']
			if resource.strip() == '':
				resource = username
			if (end_date < start_date) == True:
				message = 'End date can not be less than start date'
				return render_to_response('resource.template',{'form': form,'username':username,'resource':resource})
			t_start = time_old.time()
			(resource_model,resource_metrics) = calendar.getFeed(start_date=start_date,end_date=end_date,email=resource,text_query=text_query)
			t_end = time_old.time()
			execution_time = '%.5f' % (t_end-t_start)
			log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/resource'}
			log_message = '[email] %s [text_query] %s [start_date] %s [end_date] %s [entries] %d [execution_time] %s seconds' % (resource,text_query,start_date,end_date,len(resource_model),execution_time)
			logger.info(log_message,extra=log_params)
			request.session['resource_model'] = resource_model
			request.session['resource_metrics'] = resource_metrics
			name = getNameFromModel(resource,resource_model)
			return render_to_response('resource.template', {'form': form, 'resource_model': resource_model,'resource_metrics':resource_metrics,'username':username,'execution_time':execution_time,'name':name})
			
	return render_to_response('resource.template',{'form': form,'username':username,'email':email})

# -----------------------------------------------------------------------------
def compare(request):
# -----------------------------------------------------------------------------

	calendar = request.session.get('calendar', None)
	if calendar == None or calendar.authenticated() == False:
		return HttpResponseRedirect('/login')

	username = calendar.username
	email = None

	if request.method == 'GET':
		form = CompareForm()
	elif request.method == 'POST':
		request.session['compare_name1'] = None
		request.session['compare_email1'] = None
		request.session['compare_metrics1'] = None
		request.session['compare_name2'] = None
		request.session['compare_email2'] = None
		request.session['compare_metrics2'] = None
		request.session['compare_model'] = None
		form = CompareForm(request.POST)
		if form.is_valid():
			start_date = form.cleaned_data['start_date']
			end_date = form.cleaned_data['end_date'] + timedelta(days=1)
			text_query = form.cleaned_data['text_query']
			compare_email1 = form.cleaned_data['compare_email1']
			compare_email2 = form.cleaned_data['compare_email2']
			if (end_date < start_date) == True:
				message = 'End date can not be less than start date'
				return render_to_response('compare.template',{'form': form,'username':username})
			t_start = time_old.time()
			(compare_model1,compare_metrics1) = calendar.getFeed(start_date=start_date,end_date=end_date,email=compare_email1,text_query=text_query,)
			(compare_model2,compare_metrics2) = calendar.getFeed(start_date=start_date,end_date=end_date,email=compare_email2,text_query=text_query)
			compare_model = getCommonMeetings(compare_model1,compare_model2)
			compare_name1 = getNameFromModel(compare_email1,compare_model1)
			compare_name2 = getNameFromModel(compare_email2,compare_model2)
			request.session['compare_name1'] = compare_name1
			request.session['compare_email1'] = compare_email1
			request.session['compare_metrics1'] = compare_metrics1
			request.session['compare_name2'] = compare_name2
			request.session['compare_email2'] = compare_email2
			request.session['compare_metrics2'] = compare_metrics2
			request.session['compare_model'] = compare_model
			t_end = time_old.time()
			execution_time = '%.5f' % (t_end-t_start)
			log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/compare'}
			log_message = '[email1] %s [email2] %s [text_query] %s [start_date] %s [end_date] %s [entries] %d [execution_time] %s seconds' % (compare_email1,compare_email2,text_query,start_date,end_date,(len(compare_model1)+len(compare_model2)),execution_time)
			logger.info(log_message,extra=log_params)
			return render_to_response('compare.template', {'form': form, 'compare_metrics1': compare_metrics1,'compare_metrics2':compare_metrics2,'compare_name1':compare_name1,'compare_name2':compare_name2,'execution_time':execution_time,'username':username,'compare_model':compare_model})
			
	return render_to_response('compare.template',{'form': form,'username':username})	

# -----------------------------------------------------------------------------
def getCommonMeetings(model1, model2):
# -----------------------------------------------------------------------------

	if model1 == None or model2 == None:
		return []
		
	model = []
	
	for event1 in model1:
		for event2 in model2:
			if event1.id == event2.id:
				if event1 not in model:
					model.append(event1)

	return model
	
# -----------------------------------------------------------------------------
def details(request, model_source, event_id):
# -----------------------------------------------------------------------------

	#username = request.session.get('username', None)

	calendar = request.session.get('calendar', None)
	if calendar == None or calendar.authenticated() == False:
		return HttpResponseRedirect('/login')

	username = calendar.username
	email = None
	model = request.session.get('model', None)
	resource_model = request.session.get('resource_model', None)
	compare_model = request.session.get('compare_model', None)
	
	if username == None:
		return HttpResponseRedirect('/login')

	if model_source == 'dashboard' and model == None:
		return HttpResponseRedirect('/dashboard')

	if model_source == 'resource' and resource_model == None:
		return HttpResponseRedirect('/resource')

	if model_source == 'compare' and compare_model == None:
		return HttpResponseRedirect('/compare')

	if model_source == 'dashboard':
		details_model = model

	if model_source == 'resource':
		details_model = resource_model

	if model_source == 'compare':
		details_model = compare_model
		
	event = None
	for model_entry in details_model:
		if model_entry.id == event_id:
			event = model_entry
			break

	log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/%s/details'%(model_source)}
	log_message = '[event] %s (%s)' % (event.title,event.start_time)
	logger.info(log_message,extra=log_params)

	return render_to_response('details.template', {'event': event,'username':username})

# -----------------------------------------------------------------------------
def chart(request, model_source, chart_id, chart_x, chart_y):
# -----------------------------------------------------------------------------

	log_params = {'clientip':getRemoteAddress(request),'username':getUsername(request),'method':'/%s/chart'%(model_source)}
	log_message = '[chart_id] %s [x] %s [y] %s' % (chart_id,chart_x,chart_y)
	logger.info(log_message,extra=log_params)

	return render_to_response('chart.template', {'model_source':model_source, 'chart_id': chart_id, 'chart_x':chart_x, 'chart_y':chart_y})
	
# -----------------------------------------------------------------------------
def chartdata(request, model_source, chart_id):
# -----------------------------------------------------------------------------

	if chart_id == 'compare':
		return chartdata_compare(request, model_source)
	elif chart_id == 'dayofweek':
		return chartdata_dayofweek(request, model_source)
	elif chart_id == 'weekofyear':
		return chartdata_weekofyear(request, model_source)
	elif chart_id == 'count':	
		return chartdata_count(request, model_source)
	elif chart_id == 'cost':	
		return chartdata_cost(request, model_source)
	elif chart_id == 'time':	
		return chartdata_time(request, model_source)
	elif chart_id == 'favorganizer':	
		return chartdata_favorganizer(request, model_source)
	elif chart_id == 'favinvitee':	
		return chartdata_favinvitee(request, model_source)
	elif chart_id == 'favcoattendees':	
		return chartdata_favcoattendees(request, model_source)
	else:
		return HttpResponse('{}')

# -----------------------------------------------------------------------------
def getNameFromModel(email, model):
# -----------------------------------------------------------------------------

	if model:
		for event in model:
			if event.organizer.email == email:
				return event.organizer.name
			for attendee in event.attendees:
				if attendee.email == email:
					return attendee.name
		return email
	else:
		return email

# -----------------------------------------------------------------------------
class LoginForm(forms.Form):
# -----------------------------------------------------------------------------

    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

# -----------------------------------------------------------------------------
class DashboardForm(forms.Form):
# -----------------------------------------------------------------------------

	years = [2007,2008,2009,2010,2011,2012,2013]
	email = forms.EmailField(required=False)
	text_query = forms.CharField(required=False)
	start_date = forms.DateField(widget=SelectDateWidget(years=years),initial=date.today)
	end_date = forms.DateField(widget=SelectDateWidget(years=years),initial=date.today)

# -----------------------------------------------------------------------------
class ResourceForm(forms.Form):
# -----------------------------------------------------------------------------

	resource = forms.ChoiceField(choices=RESOURCE_LIST,required=True)
	text_query = forms.CharField(required=False)
	start_date = forms.DateField(widget=SelectDateWidget(years=VALID_YEARS),initial=date.today)
	end_date = forms.DateField(widget=SelectDateWidget(years=VALID_YEARS),initial=date.today)

# -----------------------------------------------------------------------------
class CompareForm(forms.Form):
# -----------------------------------------------------------------------------

	years = [2007,2008,2009,2010,2011,2012,2013]
	compare_email1 = forms.EmailField(required=True)
	compare_email2 = forms.EmailField(required=True)
	text_query = forms.CharField(required=False)
	start_date = forms.DateField(widget=SelectDateWidget(years=VALID_YEARS),initial=date.today)
	end_date = forms.DateField(widget=SelectDateWidget(years=VALID_YEARS),initial=date.today)


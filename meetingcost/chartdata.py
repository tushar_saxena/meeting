import locale
from datetime import *
from pyofc2  import * 
from django.http import HttpResponse

CHART_BG_COLOR = '#ffffff'
CHART_RED = '#ff0000'
CHART_GREEN = '#00cc00'
CHART_BLUE = '#0000ff'

CHART_ORGANIZED = '#888888'
CHART_ACCEPTED = '#00cc00'
CHART_INVITED = '#0000cc'
CHART_TENTATIVE = '#cccc00'
CHART_DECLINED = '#cc0000'

COMPARE_CHART_TYPE = 1
CHART_COMPARE1 = '#ff0000'
CHART_COMPARE2 = '#0000ff'

locale.setlocale(locale.LC_ALL, 'en_US')

# -----------------------------------------------------------------------------
def getModel(request, model_source):
# -----------------------------------------------------------------------------

	if model_source == 'dashboard':
		model = request.session.get('model', None)
	elif model_source == 'resource':
		model = request.session.get('resource_model', None)
	else:
		return None

# -----------------------------------------------------------------------------
def getMetrics(request, model_source):
# -----------------------------------------------------------------------------

	if model_source == 'dashboard':
		return request.session.get('metrics', None)
	elif model_source == 'resource':
		return request.session.get('resource_metrics', None)
	else:
		return []

# -----------------------------------------------------------------------------
def chartdata_compare(request, model_source):
# -----------------------------------------------------------------------------

	compare_name1 = request.session.get('compare_name1', None)
	compare_name2 = request.session.get('compare_name2', None)
	compare_email1 = request.session.get('compare_email1', None)
	compare_email2 = request.session.get('compare_email2', None)
	compare_metrics1 = request.session.get('compare_metrics1', None)
	compare_metrics2 = request.session.get('compare_metrics2', None)

	if compare_metrics1 == None or compare_metrics2 == None or compare_metrics1.count ==0 or compare_metrics2.count == 0:
		return HttpResponse('{}')

	if COMPARE_CHART_TYPE == 1:	

		# Set up bars
		bar_count = bar_glass()
		bar_count.text = 'Count'
		bar_count.colour = CHART_BLUE
		bar_count.on_show = bar_on_show(type='grow-up',cascade=1,delay=0)
		bar_count.values = []	
	
		bar_cost = bar_glass()
		bar_cost.text = 'Cost'
		bar_cost.colour = CHART_RED
		bar_cost.on_show = bar_on_show(type='grow-up',cascade=1,delay=0.5)	
		bar_cost.values = []	
	
		bar_time = bar_glass()
		bar_time.text = 'Time'
		bar_time.colour = CHART_GREEN
		bar_time.on_show = bar_on_show(type='grow-up',cascade=1,delay=1)	
		bar_time.values = []	
	
		chart_labels = []
		max_count = 0.0
		max_time = 0.0
		max_cost = 0.0
		
		# Build chart
		count1 = float(compare_metrics1.count) / (float(compare_metrics1.count)+float(compare_metrics2.count)) * 100.0
		count2 = float(compare_metrics2.count) / (float(compare_metrics1.count)+float(compare_metrics2.count)) * 100.0
		cost1 = float(compare_metrics1.cost) / (float(compare_metrics1.cost)+float(compare_metrics2.cost)) * 100.0
		cost2 = float(compare_metrics2.cost) / (float(compare_metrics1.cost)+float(compare_metrics2.cost)) * 100.0
		time1 = float(compare_metrics1.time) / (float(compare_metrics1.time)+float(compare_metrics2.time)) * 100.0
		time2 = float(compare_metrics2.time) / (float(compare_metrics1.time)+float(compare_metrics2.time)) * 100.0
		
		label = '%s<br>%s' % (compare_name1,compare_email1)
		bar_count.values.append(barvalue(top=count1,tip='%d | %.2f%%<br>%s'%(compare_metrics1.count,count1,label)))
		bar_cost.values.append(barvalue(top=cost1,tip='Rs. %s | %.2f%%<br>%s'%(locale.format("%d",int(compare_metrics1.cost),grouping=True),cost1,label)))
		bar_time.values.append(barvalue(top=time1,tip='%.2f hours | %.2f%%<br>%s'%(compare_metrics1.time,time1,label)))
		chart_labels.append(compare_name1)
	
		label = '%s<br>%s' % (compare_name2,compare_email2)
		bar_count.values.append(barvalue(top=count2,tip='%d | %.2f%%<br>%s'%(compare_metrics2.count,count2,label)))
		bar_cost.values.append(barvalue(top=cost2,tip='Rs. %s | %.2f%%<br>%s'%(locale.format("%d",int(compare_metrics2.cost),grouping=True),cost2,label)))
		bar_time.values.append(barvalue(top=time2,tip='%.2f hours | %.2f%%<br>%s'%(compare_metrics2.time,time2,label)))
		chart_labels.append(compare_name2)	
		
		# Axes
		x = x_axis()
		x.min = 0
		#x.max = len(chart_labels)
		x.steps = 1
		x.labels = labels(labels=chart_labels)
	
		y = y_axis()
		y.min = 0
		y.max = 100
		y.steps = 10
	
		# Create chart and add elements
		chart = open_flash_chart()
		#chart.title = title(text='Meeting Comparison Analysis\n%s v/s %s' % (compare_email1,compare_email2),style="{font-size:24px; color:#999999; text-align:center;}")
		chart.bg_colour = CHART_BG_COLOR
		chart.add_element(bar_count)
		chart.add_element(bar_cost)
		chart.add_element(bar_time)
		chart.x_axis = x
		chart.y_axis = y
	
		return HttpResponse(chart.render())

	elif COMPARE_CHART_TYPE == 2:

		# Set up bars
		bar_compare = bar_stack()
		bar_compare.values = []	

		# Plot chart data
		count1 = float(compare_metrics1.count) / (float(compare_metrics1.count)+float(compare_metrics2.count)) * 100.0
		count2 = float(compare_metrics2.count) / (float(compare_metrics1.count)+float(compare_metrics2.count)) * 100.0
		cost1 = float(compare_metrics1.cost) / (float(compare_metrics1.cost)+float(compare_metrics2.cost)) * 100.0
		cost2 = float(compare_metrics2.cost) / (float(compare_metrics1.cost)+float(compare_metrics2.cost)) * 100.0
		time1 = float(compare_metrics1.time) / (float(compare_metrics1.time)+float(compare_metrics2.time)) * 100.0
		time2 = float(compare_metrics2.time) / (float(compare_metrics1.time)+float(compare_metrics2.time)) * 100.0
		
		b11 = barvalue(value=count1,colour=CHART_COMPARE1,tip='%d | %.2f%%<br>%s<br>%s<br>Count'%(compare_metrics1.count,count1,compare_name1,compare_email1))
		b12 = barvalue(value=count2,colour=CHART_COMPARE2,tip='%d | %.2f%%<br>%s<br>%s<br>Count'%(compare_metrics2.count,count2,compare_name2,compare_email2))

		b21 = barvalue(value=cost1,colour=CHART_COMPARE1,tip='Rs. %s | %.2f%%<br>%s<br>%s<br>Cost'%(locale.format("%d",int(compare_metrics1.cost),grouping=True),cost1,compare_name1,compare_email1))
		b22 = barvalue(value=cost2,colour=CHART_COMPARE2,tip='Rs. %s | %.2f%%<br>%s<br>%s<br>Cost'%(locale.format("%d",int(compare_metrics2.cost),grouping=True),cost2,compare_name2,compare_email2))

		b31 = barvalue(value=time1,colour=CHART_COMPARE1,tip='%.2f hours | %.2f%%<br>%s<br>%s<br>Time'%(compare_metrics1.time,time1,compare_name1,compare_email1))
		b32 = barvalue(value=time2,colour=CHART_COMPARE2,tip='%.2f hours | %.2f%%<br>%s<br>%s<br>Time'%(compare_metrics2.time,time2,compare_name2,compare_email2))
		bar_compare.values = [[b11,b12],[b21,b22],[b31,b32]]
		bar_compare.keys = [key(text=compare_email1,colour=CHART_COMPARE1),key(text=compare_email2,colour=CHART_COMPARE2,font_size=12)]
		bar_compare.colours = [CHART_COMPARE1,CHART_COMPARE2]
		bar_compare.text = 'Hello'

		# Axes
		x = x_axis()
		x.min = 0
		x.steps = 1
		x.labels = labels(labels=['Count','Cost','Time'])
	
		y = y_axis()
		y.min = 0
		y.max = 100
		y.steps = 50
	
		# Create chart and add elements
		chart = open_flash_chart()
		#chart.title = title(text='Meeting Comparison Analysis\n(red) %s v/s %s (blue)\n\n' % (compare_email1,compare_email2),style="{font-size:24px; color:#999999; text-align:center;}")
		chart.bg_colour = CHART_BG_COLOR
		chart.add_element(bar_compare)
		chart.x_axis = x
		chart.y_axis = y

		json_response = chart.render().replace('"value":','"val":')
		json_response = json_response.replace('"elements": [{','"elements": [{"on-show": {"type": "drop", "cascade": 1, "delay": 0.5}, ')

		return HttpResponse(json_response)

# -----------------------------------------------------------------------------
def chartdata_dayofweek(request, model_source):
# -----------------------------------------------------------------------------

	metrics = getMetrics(request,model_source)
	if metrics == None:
		return HttpResponse('{}')

	# Set up bars
	bar_count = bar_glass()
	bar_count.text = 'Count'
	bar_count.colour = CHART_BLUE
	bar_count.on_show = bar_on_show(type='grow-up',cascade=0,delay=0)
	bar_count.values = []	

	bar_cost = bar_glass()
	bar_cost.text = 'Cost'
	bar_cost.colour = CHART_RED
	bar_cost.on_show = bar_on_show(type='grow-up',cascade=0,delay=0.5)	
	bar_cost.values = []	

	bar_time = bar_glass()
	bar_time.text = 'Time'
	bar_time.colour = CHART_GREEN
	bar_time.on_show = bar_on_show(type='grow-up',cascade=0,delay=1)	
	bar_time.values = []	

	chart_labels = []
	max_count = 0.0
	max_time = 0.0
	max_cost = 0.0
	
	# Get max values (for normalization)
	for entry in metrics.dayofweek:
		if entry.count > max_count:
			max_count = entry.count
		if entry.time_ > max_time:
			max_time = entry.time_
		if entry.cost > max_cost:
			max_cost = entry.cost

	# Loop through entries
	for entry in metrics.dayofweek:
		bar_count.values.append(barvalue(top=(entry.count/max_count*100),tip='%.2f<br>'%(entry.count)))
		bar_cost.values.append(barvalue(top=(entry.cost/max_cost*100),tip='Rs. %s<br>'%(locale.format("%d",entry.cost,grouping=True))))
		bar_time.values.append(barvalue(top=(entry.time_/max_time*100),tip='%.2f hours<br>'%(entry.time_)))
		chart_labels.append(entry.day)
	
	# Axes
	x = x_axis()
	x.min = 0
	#x.max = len(chart_labels) - 1
	x.steps = 1
	x.labels = labels(labels=chart_labels)

	y = y_axis()
	y.min = 0
	y.max = 100
	y.steps = 10

	# Create chart and add elements
	chart = open_flash_chart()
	chart.title = title(text='Day of Week (Avg)',style="{font-size:24px; color:#999999; text-align:center;}")
	chart.bg_colour = CHART_BG_COLOR
	chart.add_element(bar_count)
	chart.add_element(bar_cost)
	chart.add_element(bar_time)
	chart.x_axis = x
	chart.y_axis = y

	return HttpResponse(chart.render())

# -----------------------------------------------------------------------------
def chartdata_weekofyear(request, model_source):
# -----------------------------------------------------------------------------

	metrics = getMetrics(request,model_source)
	if metrics == None:
		return HttpResponse('{}')

	# Set up bars
	bar_count = bar_glass()
	bar_count.text = 'Count'
	bar_count.colour = CHART_BLUE
	bar_count.on_show = bar_on_show(type='grow-up',cascade=0,delay=0)
	bar_count.values = []	

	bar_cost = bar_glass()
	bar_cost.text = 'Cost'
	bar_cost.colour = CHART_RED
	bar_cost.on_show = bar_on_show(type='grow-up',cascade=0,delay=0.5)
	bar_cost.values = []	

	bar_time = bar_glass()
	bar_time.text = 'Time'
	bar_time.colour = CHART_GREEN
	bar_time.on_show = bar_on_show(type='grow-up',cascade=0,delay=1)
	bar_time.values = []	

	chart_labels = []
	max_count = 0.0
	max_time = 0.0
	max_cost = 0.0
	
	# Get max values (for normalization)
	for entry in metrics.weekofyear:
		if entry.count > max_count:
			max_count = entry.count
		if entry.time_ > max_time:
			max_time = entry.time_
		if entry.cost > max_cost:
			max_cost = entry.cost

	# Loop through entries
	for entry in metrics.weekofyear:
		start_of_week = str(fromIsoCalendar(int(entry.year),int(entry.week),1))[:10]
		end_of_week = str(fromIsoCalendar(int(entry.year),int(entry.week),1) + timedelta(6))[:10]

		label = '%s<br>%s' % (start_of_week,end_of_week)
		bar_count.values.append(barvalue(top=(entry.count/max_count*100),tip='%d<br>%s'%(entry.count,label)))
		bar_cost.values.append(barvalue(top=(entry.cost/max_cost*100),tip='#x_label#Rs. %s<br>%s'%(locale.format("%d",entry.cost,grouping=True),label)))
		bar_time.values.append(barvalue(top=(entry.time_/max_time*100),tip='%.2f hours<br>%s'%(entry.time_,label)))
		chart_labels.append('W%02d<br>%d'%(int(entry.week),int(entry.year)))
	
	# Axes
	x = x_axis()
	x.min = 0
	#x.max = len(chart_labels)
	x.steps = 1
	x.labels = labels(labels=chart_labels)

	y = y_axis()
	y.min = 0
	y.max = 100
	y.steps = 10

	# Create chart and add elements
	chart = open_flash_chart()
	chart.title = title(text='Week of Year',style="{font-size:24px; color:#999999; text-align:center;}")
	chart.bg_colour = CHART_BG_COLOR
	chart.add_element(bar_count)
	chart.add_element(bar_cost)
	chart.add_element(bar_time)
	chart.x_axis = x
	chart.y_axis = y

	return HttpResponse(chart.render())

# -----------------------------------------------------------------------------
def chartdata_count(request, model_source):
# -----------------------------------------------------------------------------

	metrics = getMetrics(request,model_source)
	if metrics == None:
		return HttpResponse('{}')

	# Set up pie slices
	p = pie()

	organized_percentage = '%.2f' % (metrics.count_organized/metrics.count * 100)
	pv1 = pie_value(value=metrics.count_organized,label='Organized',tip='#val# | %.2f%%<br>#label#' % (metrics.count_organized/metrics.count * 100),color=CHART_ORGANIZED)
	pv2 = pie_value(value=metrics.count_accepted,label='Accepted',tip='#val# | %.2f%%<br>#label#' % (metrics.count_accepted/metrics.count * 100),color=CHART_ACCEPTED)
	pv3 = pie_value(value=metrics.count_invited,label='Invited',tip='#val# | %.2f%%<br>#label#' % (metrics.count_invited/metrics.count * 100),color=CHART_INVITED)
	pv4 = pie_value(value=metrics.count_tentative,label='Tentative',tip='#val# | %.2f%%<br>#label#' % (metrics.count_tentative/metrics.count * 100),color=CHART_TENTATIVE)
	pv5 = pie_value(value=metrics.count_declined,label='Declined',tip='#val# | %.2f%%<br>#label#' % (metrics.count_declined/metrics.count * 100),color=CHART_DECLINED)
	if model_source != 'resource':
		p.values = [pv1,pv2,pv3,pv4,pv5]
	else:
		p.values = [pv1,pv2,pv5]
	p['animate'] = [{'type':'fade'},{'type':'bounce','distance':5}]

	# Create chart and add elements
	chart = open_flash_chart()
	chart.title = title(text='Number of Meetings\n%d'%metrics.count,style="{font-size:24px; color:#999999; text-align:center;}")
	chart.bg_colour = CHART_BG_COLOR
	chart.add_element(p)

	return HttpResponse(chart.render())		

# -----------------------------------------------------------------------------
def chartdata_cost(request, model_source):
# -----------------------------------------------------------------------------

	metrics = getMetrics(request,model_source)
	if metrics == None:
		return HttpResponse('{}')

	# Set up pie slices
	p = pie()

	organized_percentage = '%.2f' % (metrics.cost_organized/metrics.cost * 100)
	pv1 = pie_value(value=int(metrics.cost_organized),label='Organized',tip='Rs. #val# | %.2f%%<br>#label#' % (metrics.cost_organized/metrics.cost * 100),color=CHART_ORGANIZED)
	pv2 = pie_value(value=int(metrics.cost_accepted),label='Accepted',tip='Rs. #val# | %.2f%%<br>#label#' % (metrics.cost_accepted/metrics.cost * 100),color=CHART_ACCEPTED)
	pv3 = pie_value(value=int(metrics.cost_invited),label='Invited',tip='Rs. #val# | %.2f%%<br>#label#' % (metrics.cost_invited/metrics.cost * 100),color=CHART_INVITED)
	pv4 = pie_value(value=int(metrics.cost_tentative),label='Tentative',tip='Rs. #val# | %.2f%%<br>#label#' % (metrics.cost_tentative/metrics.cost * 100),color=CHART_TENTATIVE)
	pv5 = pie_value(value=int(metrics.cost_declined),label='Declined',tip='Rs. #val# | %.2f%%<br>#label#' % (metrics.cost_declined/metrics.cost * 100),color=CHART_DECLINED)
	if model_source != 'resource':
		p.values = [pv1,pv2,pv3,pv4,pv5]
	else:
		p.values = [pv1,pv2,pv5]
	p['animate'] = [{'type':'fade'},{'type':'bounce','distance':5}]

	# Create chart and add elements
	chart = open_flash_chart()
	chart.title = title(text='Cost of Meetings\nRs. %s'%locale.format("%d",metrics.cost,grouping=True),style="{font-size:24px; color:#999999; text-align:center;}")
	chart.bg_colour = CHART_BG_COLOR
	chart.add_element(p)

	return HttpResponse(chart.render())		

# -----------------------------------------------------------------------------
def chartdata_time(request, model_source):
# -----------------------------------------------------------------------------

	metrics = getMetrics(request,model_source)
	if metrics == None:
		return HttpResponse('{}')

	# Set up pie slices
	p = pie()

	organized_percentage = '%.2f' % (metrics.time_organized/metrics.time * 100)
	pv1 = pie_value(value=metrics.time_organized,label='Organized',tip='#val# hours | %.2f%%<br>#label#' % (metrics.time_organized/metrics.time * 100),color=CHART_ORGANIZED)
	pv2 = pie_value(value=metrics.time_accepted,label='Accepted',tip='#val# hours | %.2f%%<br>#label#' % (metrics.time_accepted/metrics.time * 100),color=CHART_ACCEPTED)
	pv3 = pie_value(value=metrics.time_invited,label='Invited',tip='#val# hours | %.2f%%<br>#label#' % (metrics.time_invited/metrics.time * 100),color=CHART_INVITED)
	pv4 = pie_value(value=metrics.time_tentative,label='Tentative',tip='#val# hours | %.2f%%<br>#label#' % (metrics.time_tentative/metrics.time * 100),color=CHART_TENTATIVE)
	pv5 = pie_value(value=metrics.time_declined,label='Declined',tip='#val# hours | %.2f%%<br>#label#' % (metrics.time_declined/metrics.time * 100),color=CHART_DECLINED)
	if model_source != 'resource':
		p.values = [pv1,pv2,pv3,pv4,pv5]
	else:
		p.values = [pv1,pv2,pv5]
	p['animate'] = [{'type':'fade'},{'type':'bounce','distance':5}]

	# Create chart and add elements
	chart = open_flash_chart()
	chart.title = title(text='Time Spent in Meetings\n%.2f hours'%metrics.time,style="{font-size:24px; color:#999999; text-align:center;}")
	chart.bg_colour = CHART_BG_COLOR
	chart.add_element(p)

	return HttpResponse(chart.render())		

# -----------------------------------------------------------------------------
def chartdata_favorganizer(request, model_source):
# -----------------------------------------------------------------------------

	metrics = getMetrics(request,model_source)
	if metrics == None:
		return HttpResponse('{}')

	chart_title = None
	chart_legend = None
	if model_source == 'resource':
		chart_title = 'Frequent Organizers'
		chart_legend = 'Who booked this resource the most?'
	else:
		chart_title = 'Favorite Organizer'
		chart_legend = 'Who invited you to most meetings?'

	# Set up bars
	bar_count = bar_glass()
	bar_count.text = chart_legend
	bar_count.colour = CHART_BLUE
	bar_count.on_show = bar_on_show(type='grow-up',cascade=1,delay=0)
	bar_count.values = []	

	chart_labels = []
	max_count = 0.0
	
	# Get max values (for normalization)
	for entry in metrics.favorite_organizer:
		if entry.value > max_count:
			max_count = entry.value

	if max_count == 0:
		max_count = 1

	# Loop through entries
	for entry in metrics.favorite_organizer:
		bar_count.values.append(barvalue(top=(entry.value),tip='%d<br>%s<br>%s'%(entry.value,entry.name,entry.email)))
		entry_name = entry.name.split('(')[0].strip()
		entry_initials = ('%s%s' % (entry_name.split()[0][0],entry_name.split()[-1][0])).upper()
		chart_labels.append(entry_initials)
	
	# Axes
	x = x_axis()
	x.min = 0
	#x.max = len(chart_labels) - 1
	x.steps = 1
	x.labels = labels(labels=chart_labels)

	y = y_axis()
	y.min = 0
	y.max = max_count
	if y.max > 100: y.steps = int(y.max/10)
	else: y.steps = 10

	# Create chart and add elements
	chart = open_flash_chart()
	chart.title = title(text=chart_title,style="{font-size:24px; color:#999999; text-align:center;}")
	chart.bg_colour = CHART_BG_COLOR
	chart.add_element(bar_count)
	chart.x_axis = x
	chart.y_axis = y

	return HttpResponse(chart.render())

# -----------------------------------------------------------------------------
def chartdata_favinvitee(request, model_source):
# -----------------------------------------------------------------------------

	metrics = getMetrics(request,model_source)
	if metrics == None:
		return HttpResponse('{}')

	# Set up bars
	bar_count = bar_glass()
	bar_count.text = 'Who did you invite to most meetings?'
	bar_count.colour = CHART_RED
	bar_count.on_show = bar_on_show(type='grow-up',cascade=1,delay=0)
	bar_count.values = []	

	chart_labels = []
	max_count = 0.0
	
	# Get max values (for normalization)
	for entry in metrics.favorite_invitee:
		if entry.value > max_count:
			max_count = entry.value

	if max_count == 0:
		max_count = 1

	# Loop through entries
	for entry in metrics.favorite_invitee:
		bar_count.values.append(barvalue(top=(entry.value),tip='%d<br>%s<br>%s'%(entry.value,entry.name,entry.email)))
		entry_name = entry.name.split('(')[0].strip()
		entry_initials = ('%s%s' % (entry_name.split()[0][0],entry_name.split()[-1][0])).upper()
		chart_labels.append(entry_initials)
	
	# Axes
	x = x_axis()
	x.min = 0
	#x.max = len(chart_labels) - 1
	x.steps = 1
	x.labels = labels(labels=chart_labels)

	y = y_axis()
	y.min = 0
	y.max = max_count
	if y.max > 100: y.steps = int(y.max/10)
	else: y.steps = 10

	# Create chart and add elements
	chart = open_flash_chart()
	chart.title = title(text='Favorite Invitee',style="{font-size:24px; color:#999999; text-align:center;}")
	chart.bg_colour = CHART_BG_COLOR
	chart.add_element(bar_count)
	chart.x_axis = x
	chart.y_axis = y

	return HttpResponse(chart.render())

# -----------------------------------------------------------------------------
def chartdata_favcoattendees(request, model_source):
# -----------------------------------------------------------------------------

	metrics = getMetrics(request,model_source)
	if metrics == None:
		return HttpResponse('{}')

	chart_title = None
	chart_legend = None
	if model_source == 'resource':
		chart_title = 'Frequent Attendees'
		chart_legend = 'Who attended most meetings with this resource?'
	else:
		chart_title = 'Favorite Co-attendee'
		chart_legend = 'Who attended most meetings with you?'

	# Set up bars
	bar_count = bar_glass()
	bar_count.text = chart_legend
	bar_count.colour = CHART_GREEN
	bar_count.on_show = bar_on_show(type='grow-up',cascade=1,delay=0)
	bar_count.values = []	

	chart_labels = []
	max_count = 0.0
	
	# Get max values (for normalization)
	for entry in metrics.favorite_coattendees:
		if entry.value > max_count:
			max_count = entry.value

	if max_count == 0:
		max_count = 1

	# Loop through entries
	for entry in metrics.favorite_coattendees:
		bar_count.values.append(barvalue(top=(entry.value),tip='%d<br>%s<br>%s'%(entry.value,entry.name,entry.email)))
		entry_name = entry.name.split('(')[0].strip()
		entry_initials = ('%s%s' % (entry_name.split()[0][0],entry_name.split()[-1][0])).upper()
		chart_labels.append(entry_initials)
	
	# Axes
	x = x_axis()
	x.min = 0
	#x.max = len(chart_labels) - 1
	x.steps = 1
	x.labels = labels(labels=chart_labels)

	y = y_axis()
	y.min = 0
	y.max = max_count
	if y.max > 100: y.steps = int(y.max/10)
	else: y.steps = 10

	# Create chart and add elements
	chart = open_flash_chart()
	chart.title = title(text=chart_title,style="{font-size:24px; color:#999999; text-align:center;}")
	chart.bg_colour = CHART_BG_COLOR
	chart.add_element(bar_count)
	chart.x_axis = x
	chart.y_axis = y

	return HttpResponse(chart.render())

# -----------------------------------------------------------------------------
def fromIsoCalendar(y,w,d):
# -----------------------------------------------------------------------------

	if d == 7: d=0
	return datetime.strptime( "%04dW%02d-%d"%(y,w,d), "%YW%W-%w")

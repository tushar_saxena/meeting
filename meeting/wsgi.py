"""
WSGI config for meeting project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""

import os
import sys

DJANGO_PROJECT_PATH = '/Users/tushar/GIT/meeting/'
if DJANGO_PROJECT_PATH not in sys.path:
    sys.path.append(DJANGO_PROJECT_PATH)

os.environ['DJANGO_SETTINGS_MODULE'] = 'meeting.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

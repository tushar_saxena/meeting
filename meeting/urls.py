from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', 'meetingcost.views.root'),
	url(r'^login$', 'meetingcost.views.login'),
	url(r'^login/callback$', 'meetingcost.views.login_callback'),
	url(r'^logout$', 'meetingcost.views.logout'),
	url(r'^logout/purge$', 'meetingcost.views.logout_purge'),
	url(r'^dashboard$', 'meetingcost.views.dashboard'),
	url(r'^resource$', 'meetingcost.views.resource'),
	url(r'^compare$', 'meetingcost.views.compare'),
	url(r'^(?P<model_source>\w+)/details/(?P<event_id>\w+)$', 'meetingcost.views.details'),
	url(r'^(?P<model_source>\w+)/chart/(?P<chart_id>\w+)/(?P<chart_x>\w+)/(?P<chart_y>\w+)/$', 'meetingcost.views.chart'),
	url(r'^(?P<model_source>\w+)/chart/data/(?P<chart_id>\w+)/$', 'meetingcost.views.chartdata'),
	#url(r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', {'url': '/static/images/favicon.ico'}),
	#url(r'^test$', 'meetingcost.views.test'),
	#url(r'^process$', 'meetingcost.views.process'),
)

rm logs/*
find . -name *.pyc | xargs rm
rm db.sqlite
python manage.py syncdb